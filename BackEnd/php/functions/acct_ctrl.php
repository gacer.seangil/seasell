<?php
require_once __DIR__ . "/../base/connectors.php";
require_once __DIR__ . "/../base/validationsettings.php";

/** Add a username and initializing details into the table of awaiting registrees
 * 
 */
function register(string $username, string $password, string $region, string $city, string $email, string $phone, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    #TEST 1: Check if username follows rules.
    checkusernamevalidity($username);

    #TEST 2 : Check new registeree info against other registrees
    checkusernameavailability($username, $conn);

    #TARGET 3: Send to DB new information

    $stmt_in = $conn->prepare("INSERT INTO registrees VALUES (?,?,?,?,?,?)");
    $hasherino = hash("sha512", "{$username}_{$password}");
    $stmt_in->bind_param("ssssss", $username, $hasherino, $region, $city, $email, $phone);
    $stmt_in->execute();
    $conn->close();
    unset($hasherino);
    
    return "RegSuccess:Registration Success for:" . $username;
}

/** Initializes an account immediately without going through the typical email confirmation procedure
 * @param $newuserdata a string of text received from client-side. It is exploded within the function.
 *  [0] = username
 *  [1] = passhash
 *  [2] = Region
 *  [3] = City
 *  [4] = Email
 *  [5] = Phone Number
 */
function register_fasttrack(string $username, string $password, string $region, string $city, string $email, string $phone, mysqli $conn = null)
{
    //CHECK IF DEBUG SETTING IS ENABLED
    if (!DEBUG_FASTTRACK) {
        throw new Exception("DEBUG FUNCTION REGISTER_FASTTRACK DISABLED");
    }

    //Check if supplied username
    checkusernamevalidity($username);
    checkusernameavailability($username);

    //Database operations: insert new user into users table directly
    try {
        if ($conn == null) {
            $conn = create_conn_mysqli();
        }
        $stmt_in = $conn->prepare("INSERT INTO 
        users (`user_username`, `user_passhash`, `user_region`, `user_city`, `user_email`, `user_contact`) 
        VALUES (?, ?, ?, ?, ?, ?)");
        $hasherino = hash("sha512", "{$username}_{$password}");
        $stmt_in->bind_param("ssssss", $username, $hasherino, $region, $city, $email, $phone);
        $stmt_in->execute();
        unset($hasherino);
    } catch (Exception $e) {
        $conn->close();
        throw $e;
    }

    $conn->close();
    return "AcctCreateSuccess: Account Created";
}

/** Check if a registration link is valid and active. If so, move the linked registree to the table of users.
 * 
 */
function confirm_register(string $regist_ticket, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }
    // STEP 1: CHECK
    try {
        $stmt_se1 = $conn->prepare("SELECT registrees_id FROM registrees WHERE `reg_ticket` = ?");
        $stmt_se1->bind_param("s", $regist_ticket);
        $stmt_se1->bind_result($regist_id);

        $stmt_se1->execute();
        $stmt_se1->store_result();
        $count = $stmt_se1->num_rows();
        $stmt_se1->fetch();

        if ($count < 1) {
            throw new Exception("MissingReg: Registration with ticket " . $regist_ticket . " is missing.");
        }
        if ($count > 1) {
            throw new Exception("MissingReg: Registration ticket " . $regist_ticket . " collides with multiple registrations.");
        }

        $stmt_se1->close();
    } catch (Exception $ex) {
        $stmt_se1->close();
        $conn->close();
        throw ($ex);
    }
    // STEP 2: TAKE
    try {
        $stmt_se2 = $conn->prepare("SELECT reg_username, reg_passhash, reg_region, reg_city, reg_email, reg_phone FROM registrees WHERE `regist_ticket` = ?");
        $stmt_se2->bind_param("s", $regist_ticket);
        $stmt_se2->bind_result($reg_username, $reg_passhash, $reg_region, $reg_city, $reg_email);
        $stmt_se2->execute();
        $stmt_se2->fetch();
        $stmt_se2->close();
    } catch (Exception $ex) {
        $stmt_se2->close();
        $conn->close();
        throw $ex;
    }
    //STEP 3: INSERT
    try {
        $stmt_in = $conn->prepare("INSERT INTO users (`username`, `passhash`, `country`, `region`, `email`, `phone`) VALUES (?,?,?,?,?)");
        $stmt_in->bind_param($reg_username, $reg_passhash, $reg_region, $reg_city, $reg_email);
        $stmt_in->execute();
        $stmt_in->close();
        $conn->close();
    } catch (\Throwable $ex) {
        $stmt_in->close();
        $conn->close();
        throw $ex;
    }
    
    return "RegSuccess:Registree " . $reg_username . " is now regular user.";
}

/**Creates a new authkey instance for a user, and gives it to client browser.
 * 
 * @return string authkey for identity-sensitive operations
 */
function log_in(string $username, string $password, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    $userid = verify_credentialpair($username, $password, $conn);

    $raw_give_new_authkey = $conn->prepare("INSERT INTO `hashkeys` (hashkey_key, hashkey_user) VALUES (?, ?)");
    $datenow = strtotime(date("YmdHis"));
    $new_authkey = hash('sha512', "{$username}_{$password}_{$datenow}");
    $raw_give_new_authkey->bind_param("ss", $new_authkey, $userid);
    $raw_give_new_authkey->execute();
    $raw_give_new_authkey->close();

    $conn->close();

    return $new_authkey;
}

function log_in_admin(string $username, string $passhash)
{
}

function log_out(string $hashkey)
{
}

function convert_to_seller(string $username, string $authkey, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    checkalive_authkey($authkey, $username, $conn); //If this fails, it throws an exception.
}

// VALIDATION AND INQUIRIES

function checkusernamevalidity(string $username)
{

    //Test 1: Check length
    if (strlen($username) < USERNAME_LENGTH_MIN or strlen($username) > USERNAME_LENGTH_MAX) {
        throw new Exception("BadUsername:Username is too short or long (Between " . USERNAME_LENGTH_MIN . " and " . USERNAME_LENGTH_MAX . " characters).");
    }
    //Test 2: Deny symbols disallowed 
    if (preg_match(USERNAME_REGEX_RULE, $username) < 1) {
        throw new Exception("BadUsername:Username does not respect allowed name format.");
    }
    return true;
}

function checkusernameavailability(string $username, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    $raw_checkagainstusers = $conn->prepare("SELECT COUNT(*) FROM users WHERE `user_username` = ?");
    $raw_checkagainstusers->bind_param("s", $username);
    $raw_checkagainstusers->bind_result($usermatches);
    $raw_checkagainstusers->execute();
    $raw_checkagainstusers->fetch();
    if ($usermatches > 0) {
        $conn->close();
        $raw_checkagainstusers->close();
        throw new Exception("UsernameTaken: The username " . $username . " has already been taken.");
    }
    $raw_checkagainstusers->close();

    $raw_checkagainstregistrees = $conn->prepare("SELECT COUNT(*) FROM registrees WHERE `reg_username` = ?");
    $raw_checkagainstregistrees->bind_param("s", $username);
    $raw_checkagainstregistrees->bind_result($regimatches);
    $raw_checkagainstregistrees->execute();
    $raw_checkagainstregistrees->fetch();
    if ($regimatches > 0) {
        $conn->close();
        $raw_checkagainstregistrees->close();
        throw new Exception("UsernameTaken: The username " . $username . " has already been taken.");
    }

    $raw_checkagainstregistrees->close();
    $conn->close();

    return true;
}

/** Get an user's id number or username, depending on parameter arguments
 *
 * @param string $usernameorid known user identifier
 * @return string|false userid or username, or `false` if none is found.
 * 
 */
function getuseralt(string $usernameorid, $isusername = false, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    if (!$isusername) {
        $raw_getusername = $conn->prepare("SELECT user_username FROM users WHERE user_id = ?");
        $raw_getusername->bind_param("s", $usernameorid);
        $raw_getusername->bind_result($retrieved_username);
        $raw_getusername->execute();
        $raw_getusername->store_result();
        $raw_getusername->fetch();
        if ($raw_getusername->num_rows() != 1 || $retrieved_username == null || strlen($retrieved_username) < USERNAME_LENGTH_MIN) {
            $conn->close();
            return false;
        } else {
            $conn->close();
            return $retrieved_username;    
        }
    } else {
        $raw_getuserid = $conn->prepare("SELECT user_id FROM users WHERE user_username = ?");
        $raw_getuserid->bind_param("s", $usernameorid);
        $raw_getuserid->bind_result($retrieved_userid);
        $raw_getuserid->execute();
        $raw_getuserid->store_result();
        $raw_getuserid->fetch();
        if ($raw_getuserid->num_rows() < 1 || !is_numeric($retrieved_userid)) {
            return false;
        } else {
            return $retrieved_userid;    
        }
    }
}

/** Check if the authkey is still valid (alive). Throws an error if authentication fails.
 * 
 */
function checkalive_authkey($hashkey, $userid, mysqli $conn = null)
{
    if (strlen($hashkey) != 128) {
        throw new Exception("String doesn't match SHA256 length requirement! (" . $hashkey . ")");
    }
    if (!is_numeric($userid)) {
        throw new Exception("Userid isn't a number! (" . $userid . ")");
    }

    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    $stmt_checkbanstatus = $conn->prepare("SELECT `banstatus` FROM `users` WHERE user_id = ?");
    $stmt_checkbanstatus->bind_param("i", $userid);
    $stmt_checkbanstatus->bind_result($banstatus);
    $stmt_checkbanstatus->execute();
    $stmt_checkbanstatus->fetch();
    if ($banstatus == 1) {
        throw new Exception("THIS USER HAS BEEN BANNED " . $banstatus);
    }
    $stmt_checkbanstatus->close();

    $stmt_checkifexists = $conn->prepare("SELECT UNIX_TIMESTAMP(hashkey_expiry), UNIX_TIMESTAMP(NOW()), hashkey_user FROM hashkeys WHERE hashkey_key = ?");
    $stmt_checkifexists->bind_param("s", $hashkey);
    $stmt_checkifexists->bind_result($active_until, $time_now, $ownerid);

    $stmt_checkifexists->execute();
    $stmt_checkifexists->store_result();
    $match_count = $stmt_checkifexists->num_rows();
    $stmt_checkifexists->fetch();
    $stmt_checkifexists->close();

    //check if hashkey either collides with another or not
    if ($match_count > 1) {
        throw new Exception("HashkeyCollide:Session Key " . $hashkey . " has duplicates.");
    }
    if ($match_count < 1) {
        throw new Exception("HashkeyMissing:Session with key " . $hashkey . " does not exist.");
    }
    //check if hashkey is still active
    if ($active_until < $time_now) {
        throw new Exception("HashkeyExpired:Session with key " . $hashkey . " is no longer valid.");
    }
    //check if hashkey belongs to rightful owner
    if ($userid != $ownerid) {
        throw new Exception("HashkeyInvalid:Session with key " . $hashkey . " does not belong to user " . $userid . " . It belongs to user " . $ownerid);
    }
    return true;
}

function getlistofusers($adminid, $hashkey, mysqli $conn = null) //INCOMPLETE
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }
}

function verify_credentialpair($username, $password, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    if (is_null($password) or is_null($username) or $password == "" or $username == "") {
        $conn->close();
        throw new Exception("CredIncomplete:Missing one or more credentials detail.");
    }

    checkusernamevalidity($username);

    $passhash = hash("sha512", "{$username}_{$password}");

    $raw_check_credentials = $conn->prepare("SELECT `user_id` FROM `users` WHERE `user_username` = ? AND `user_passhash` = ?");
    $raw_check_credentials->bind_param("ss", $username, $passhash);
    $raw_check_credentials->bind_result($userid);

    $raw_check_credentials->execute();
    $raw_check_credentials->store_result();
    $raw_check_credentials->fetch();

    $rawcount = $raw_check_credentials->num_rows();

    $raw_check_credentials->close();

    if ($rawcount > 1) {
        throw new Exception("CredCollision:Credentials for user ID " . $username . " has collided with another.");
    }
    if ($rawcount < 1) {
        throw new Exception("CredNoMatch:Credentials for user ID " . $username . " does not match with supplied password.");
    }

    return (string) $userid;
}
