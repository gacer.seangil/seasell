<?php
require_once __DIR__ . "/../base/dbcred.php";
require_once __DIR__ . "/acct_ctrl.php";

/** adds a listing under the name of an account
 *  @param string $hashkey the session key from the client browser.
 *  @param string $u_id the userid of the account.
 *  @param array $details an array of details of the new listings. When coming from ajax POST, move relevant information from $_POST to new array.
 *  @param mysqli $conn for retaining usage of only one connection per ajax request. If none is provided, it creates one for itself.
 */
function addlisting(string $hashkey, string $u_id, array $newdetails, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    checkalive_authkey($hashkey, $u_id, $conn);

    if (!ctype_digit($u_id)) {
        throw new Exception();
    }

    $stmt = $conn->prepare("INSERT INTO listings (
        li_user,
        li_name,
        li_category,
        li_active,
        li_description,
        li_price,
        li_quantity
    ) VALUES (?,?,?,?,?,?,?)");

    $stmt->bind_param(
        "issssss",
        $u_id,
        $newdetails["name"],
        $newdetails["cat"],
        $newdetails["act"],
        $newdetails["desc"],
        $newdetails["pric"],
        $newdetails["quan"]
    );

    if (!$stmt->execute()) {
        $stmt->close();
        throw new Exception(mysqli_stmt_error($stmt));
    }
    $item_id = $stmt->insert_id;

    $stmt->close();
    $conn->close();

    return "Item " . $item_id . " successfully inserted.";
}

function attachimage_to_listing() //still need to think of a solution for this
{
}

function editlisting(string $hashkey, string $u_id, string $l_id, array $newdetails, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    checkalive_authkey($hashkey, $u_id);

    checklistingownership($l_id, $u_id, $conn);

    $stmt_up = $conn->prepare("UPDATE listings VALUES (
        li_name = ?,
        li_category = ?,
        li_active = ?,
        li_description = ?,
        li_images = ?,
        li_price = ?,
        li_quantity = ?
    ) WHERE listing_id = ?");

    $stmt_up->bind_param(
        "ssssssi",
        $newdetails["name"],
        $newdetails["cat"],
        $newdetails["act"],
        $newdetails["desc"],
        $newdetails["pric"],
        $newdetails["quan"],
        $l_id
    );

    $stmt_up->execute();
    
    return "Listing " . $l_id . " updated.";
}

function removelisting(string $hashkey, string $u_id, string $l_id, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    checkalive_authkey($hashkey, $u_id);
}


//VERIFICATION

function checklistingownership(int $l_id, int $u_id, mysqli $conn = null)
{
    if ($conn == null) {
        $conn = create_conn_mysqli();
    }

    try {
        $stmt_se = $conn->prepare("SELECT li_user FROM listings WHERE listing_id = ?");
        $stmt_se->bind_param("i", (int) $l_id);
        $stmt_se->bind_result($db_uid);
        $stmt_se->execute();
        $stmt_se->store_result();
        $stmt_se->fetch();

        if ($stmt_se->num_rows() < 1) {
            throw new Exception("Item with Listing ID " . $l_id . " not found.");
        }

        if ((int) $db_uid != (int) $u_id) {
            throw new Exception("Item with Listing ID " . $l_id . " does not belong to user " . $u_id);
        }
    } catch (Exception $ex) {
        $stmt_se->close();
        $conn->close();
        throw $ex;
    }

    return true;
}

