<?php

require_once __DIR__ . "/../auther.php";
require_once __DIR__ . "/../../functions/acct_ctrl.php";
require_once __DIR__ . "/../../base/connectors.php";

if (!auther()) {
    exit("NOAUTH");
}

$conn = create_conn_mysqli();

$stmt = $conn->prepare('WITH PREL AS (
    SELECT conversations_id as `cid`, target_listing as `target`, listings.li_user as `owner`, negotiator, m_then.content, m_then.`date`,  m_then.`type`
    FROM conversations 
    INNER JOIN listings ON listings.listing_id = target_listing
    LEFT JOIN messages m_then ON conversations_id = m_then.c_id
    LEFT OUTER JOIN messages m_now ON (
        conversations_id = m_now.c_id AND (
            m_then.messages_id < m_now.messages_id
        ))
    WHERE listings.li_user = ? OR negotiator = ?
    ), PREL2 AS (
    SELECT cid, target, listings.li_name as `item`, users.user_username as `owner`, `negotiator`, `date`, `content`, `type`
    FROM PREL
    INNER JOIN users ON `owner` = users.user_id
    INNER JOIN listings ON `target` = listings.listing_id
    )
        SELECT cid, target as `item_id`, item as `item_name`, `owner`, users.user_username as `negotiator`, `content`,`date`, `type`
        FROM PREL2
        LEFT JOIN users ON users.user_id = `negotiator`
    ;');
$userid = getuseralt($_POST["username"], true);
$stmt->bind_param("ss", $userid, $userid);
$stmt->bind_result($cid, $itemid, $itemname, $owner, $negotiator, $lastmessage, $lastwhen, $lasttype);
$stmt->execute();
$stmt->store_result();

$msgs = [];

while ($stmt->fetch()) {
    array_push($msgs, [$cid, $itemid, $itemname, $owner, $negotiator, $lastmessage, $lastwhen, $lasttype]);
}

echo json_encode($msgs);
exit();
