<?php

require_once __DIR__ . "/../auther.php";
require_once __DIR__ . "/../../base/connectors.php";

if (!auther()) {
    exit("NOAUTH");
}

$conn = create_conn_mysqli();

$itemid = $_POST["itemid"];
$uid = getuseralt($_POST["username"], true);

//Check first if an equivalent convo doesn't exist already
try {
    $stmt = $conn->prepare("SELECT conversations_id FROM `conversations` WHERE target_listing = ? AND negotiator = ?");
    $stmt->bind_param("ss", $itemid, $uid);
    $stmt->bind_result($cid);
    $stmt->execute();
    $stmt->fetch();
    if ($cid > 1) {
        $stmt->close();
        $conn->close();
        unset($cid, $stmt, $conn);
        exit("NOTE:ConvAlreadyExists " . $cid);
    }
} catch (\Exception $th) {
    exit("ERROR: " . $th->getMessage());
}

//if an existing conv doesn't exist, proceed to make one.
try {
    $stmt = $conn->prepare("INSERT INTO conversations (`target_listing`, `negotiator`) VALUES (?, ?)");
    $stmt->bind_param("ss", $itemid, $uid);
    $stmt->execute();
    $stmt->close();
    $stmt = $conn->prepare("SELECT conversations_id FROM conversations WHERE target_listing = ? AND negotiator = ?");
    $stmt->bind_param("ss", $itemid, $uid);
    $stmt->bind_result($cid);
    $stmt->execute();
    $stmt->fetch();
    echo "SUCCESS:NewConv " . $cid;
    $stmt->close();
    $conn->close();
    unset($cid, $stmt, $conn);
    exit();
} catch (\Exception $th) {
    exit("ERROR: " . $th->getMessage());
}


