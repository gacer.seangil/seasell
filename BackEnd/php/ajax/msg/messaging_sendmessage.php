<?php

require_once __DIR__ . "/../auther.php";
require_once __DIR__ . "/../../base/connectors.php";

if (!auther()) {
    exit("NOAUTH");
}

$conn = create_conn_mysqli();

$s_uid = getuseralt($_POST['username'], true, $conn);

$stmt = $conn->prepare("INSERT INTO messages (c_id, s_uid, content, `type`, `values`) VALUES (?, ?, ?, ?, ?)");
$stmt->bind_param("iisss", $_POST["convo"], $s_uid, $_POST['content'], $_POST['type'], $_POST['value']);
$stmt->execute();
$stmt->close();
$conn->close();


exit("Message sent!");