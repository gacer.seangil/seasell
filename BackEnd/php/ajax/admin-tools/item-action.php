<?php

require_once __DIR__ . "/../auther.php";

if (!auther()) {
    exit("NOAUTH");
}

try {
    $actions = [
        "reject" => "rejected",
        "accept" => "active"
    ];

    $item = $_POST["item"];
    $action = $_POST["action"];

    $conn = create_conn_mysqli();

    $stmt = $conn->prepare("UPDATE listings SET `li_status` = ? WHERE listing_id = ?");
    $stmt->bind_param("si", $actions[$action], $item);
    $stmt->execute();

    $stmt->close();
    $conn->close();
} catch (Exception $ex) {
    exit($ex->getMessage());
}

exit("ITEM UPDATED");
