<?php


static $MySQLCustomQuickQueriesArray = [
    //User Summaries
    "count_users" => "SELECT COUNT(*) FROM users",

    //Product Summaries
    "get_items_summary_all_all"      => "SELECT COUNT(listing_id) FROM listings",
    "get_items_summary_all_pending"  => "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'pending'",
    "get_items_summary_all_rejected" => "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'rejected'",
    "get_items_summary_all_active"   => "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'active'",
    "get_items_summary_all_inactive" => "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'inactive'",
    "get_items_summary_all_soldout"  => "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'soldout'",

    //Product Table Fetcher
    "get_items_all_all"      => "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY FIELD(`li_category`, 'pending', 'active', 'inactive', 'rejected', 'soldout') DESC LIMIT ? OFFSET ?;",
    "get_items_all_pending"  => "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'pending'",
    "get_items_all_rejected" => "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'rejected'",
    "get_items_all_active"   => "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'active'",
    "get_items_all_inactive" => "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'inactive'",
    "get_items_all_soldout"  => "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'soldout'",

    //Users Table Fetcher 
    "get_users" => "SELECT `user_id`, `user_username`, `user_email`, `user_country`, `user_region`, `user_city`, `account_type`, `account_elevation`, `created_at` FROM `users` WHERE `banstatus` = 0 ORDER BY user_id DESC LIMIT ? OFFSET ?;",
];

class MySQLCustomQuickQueries
{
    //RULE 1: WE DONT WANT TO BE QUERYING ALL THE ITEMS, WHICH COULD POTENTIALLY REACH THOUSANDS OF ENTRIES A QUERY. Limiting to only 15 at a time.
    public static $get_users = "SELECT * FROM users ORDER BY user_id ? LIMIT ? OFFSET ?";
    public static $count_users = "SELECT COUNT(*) FROM users";
    public static $ban_user = "UPDATE users SET user_banstatus = 1 WHERE user_id = ?";

    //This section is an exception to rule 1, since it only needs to count items, and the index provided by the primary key eases search cost.
    public static $get_items_summary_all_all        = "SELECT COUNT(listing_id) FROM listings";
    public static $get_items_summary_all_pending    = "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'pending'";
    public static $get_items_summary_all_rejected   = "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'rejected'";
    public static $get_items_summary_all_active     = "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'active'";
    public static $get_items_summary_all_inactive   = "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'inactive'";
    public static $get_items_summary_all_soldout    = "SELECT COUNT(listing_id) FROM listings WHERE li_status = 'soldout'";

    public static $get_items_all_all        = "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ?;";
    public static $get_items_all_pending    = "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'pending'";
    public static $get_items_all_rejected   = "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'rejected'";
    public static $get_items_all_active     = "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'active'";
    public static $get_items_all_inactive   = "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'inactive'";
    public static $get_items_all_soldout    = "SELECT listing_id, users.user_username, li_name, li_category, li_description, li_price, li_quantity, li_status FROM seashelldb.listings JOIN users ON li_user = users.user_id ORDER BY listing_id DESC LIMIT ? OFFSET ? WHERE li_status = 'soldout'";

    public static $get_items_bike_all       = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'bike'";
    public static $get_items_bike_pending   = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'bike' AND li_status = 'pending'";
    public static $get_items_bike_rejected  = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'bike' AND li_status = 'rejected'";
    public static $get_items_bike_active    = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'bike' AND li_status = 'active'";
    public static $get_items_bike_inactive  = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'bike' AND li_status = 'inactive'";
    public static $get_items_bike_soldout   = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'bike' AND li_status = 'soldout'";

    public static $get_items_fash_all       = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'fashion'";
    public static $get_items_fash_pending   = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'fashion' AND li_status = 'pending'";
    public static $get_items_fash_rejected  = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'fashion' AND li_status = 'rejected'";
    public static $get_items_fash_active    = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'fashion' AND li_status = 'active'";
    public static $get_items_fash_inactive  = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'fashion' AND li_status = 'inactive'";
    public static $get_items_fash_soldout   = "SELECT * FROM listings LIMIT ? OFFSET ? WHERE li_category = 'fashion' AND li_status = 'soldout'";

    public static $set_item_as_pending  = "UPDATE listing SET li_status = 'pending'";   // Item still shows up, but is low priority
    public static $set_item_as_rejected = "UPDATE listing SET li_status = 'rejected'";  // Item no longer reinstatable for offers
    public static $set_item_as_active   = "UPDATE listing SET li_status = 'active'";    // Item is available for offer, high priority
    public static $set_item_as_inactive = "UPDATE listing SET li_status = 'inactive'";  // Item is no available for offer, either by seller choice or admin judgement
    public static $set_item_as_soldout  = "UPDATE listing SET li_status = 'soldout'";   // Item is no longer supplied by seller

    public static $get_item_by_id   = "SELECT * FROM listings WHERE listing_id = ?";
    public static $get_item_by_name = "SELECT * FROM listings WHERE li_name = ?";
}
