<?php

require_once __DIR__ . "/../../base/connectors.php";
require_once __DIR__ . "/../auther.php";
require_once __DIR__ . "/./quick_queries.php";

if (!auther()) {
    exit("NOAUTH");
}

$conn = create_conn_mysqli();
$requests = $_POST["requests"];
$answers = [];

foreach ($requests as $value) {
    $stmt = $conn->prepare($MySQLCustomQuickQueriesArray[$value]);
    if (isset($_POST["bindparamtypes"])) {
        $stmt->bind_param($_POST["bindparamtypes"], ...$_POST["bindparams"]);
    }
    $stmt->execute();
    $res = $stmt->get_result();
    while ($row = $res->fetch_row()){
        array_push($answers, $row);
    }
    $res->close();
    $stmt->close();
}

exit (json_encode($answers));
