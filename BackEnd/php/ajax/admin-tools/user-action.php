<?php

require_once __DIR__ . "/../auther.php";

if (!auther()) {
    exit("NOAUTH");
}

try {
    $actions = [
        "ban" => 1,
        "unban" => 0
    ];

    $user = $_POST["user"];
    $action = $_POST["action"];

    $conn = create_conn_mysqli();

    $stmt = $conn->prepare("UPDATE users SET `banstatus` = ? WHERE `user_id` = ?");
    $stmt->bind_param("ii", $actions[$action], $user);
    $stmt->execute();

    $stmt->close();
    $conn->close();
} catch (Exception $ex) {
    exit($ex->getMessage());
}

exit("USER UPDATED");
