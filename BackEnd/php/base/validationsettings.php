<?php
const USERNAME_LENGTH_MIN = 8;
const USERNAME_LENGTH_MAX = 20;
const USERNAME_REGEX_RULE = "/^[a-zA-Z][a-zA-Z0-9]*[._-]?[a-zA-Z0-9]+$/";

/** Allow usage of `register_fasttrack()` for debugging purposes
 * Turn off in production releases.
 */
const DEBUG_FASTTRACK = TRUE;


?>