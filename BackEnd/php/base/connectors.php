<?php 
require_once __DIR__ . "/dbcred.php";
require_once __DIR__ . "/ftpcred.php";

class ftpi
{
    private $conn = null;
    private $homedirectory = null;

    public function __construct($server, $port, $username, $password, $timeout = 90)
    {
        try {
            $this->conn = ftp_connect($server, $port, $timeout);
            if (!$this->conn) {
                throw new Exception("Cannot connect to FTP server.");
            }
        } catch (Exception $ex) {
            throw $ex;
        }
        try {
            if (!ftp_login($this->conn, $username, $password)) {
                throw new Exception("Cannot login to FTP server.");
            }
        } catch (Exception $ex) {
            ftp_close($this->conn);
            throw $ex;
        }
        try {
            $this->homedirectory = ftp_pwd($this->conn);
            if (!$this->homedirectory) {
                throw new Exception("Cannot obtain root folder of FTP account.");
            }
        } catch (Exception $ex) {
            ftp_close($this->conn);
            throw $ex;
        }
    }

    public function close()
    {
        ftp_close($this->conn);
    }
}

function create_conn_ftpi() {
    return new ftpi(FTP_SERVER, FTP_PORT, FTP_USERNAME, FTP_PASSWORD);
}

function create_conn_mysqli()
{
    return new mysqli(MYSQLI_SERVERNAME, MYSQLI_USERNAME, MYSQLI_PASSWORD, MYSQLI_DATABASE);
}