export default function arraygrouper(array, subarraysize) {
    if (array.length < subarraysize) {
        throw "Array is not large enough (" + array.length + ") for given subarray size (" + subarraysize + ")!";
    }
    if (array.length == subarraysize) {
        console.warn("Warning: Array is only just as large as subarraysize , might lead to unwanted extra dimensions to array.");
    }
    if (array.length % subarraysize > 0) {
        console.warn("Warning: Array (length " + array.length + ") does not divide evenly to subarray size (" + subarraysize + "). Inserting `null` values.");
    }
    let newarray = [];
    for (let i = 0, j = 0; i < array.length; i += subarraysize, j++) {
        let newsubarray = [];
        for (let k = 0; k < subarraysize; k++) {
            newsubarray.push(array[i + k]);
        }
        newarray.push(newsubarray);
    }
    return newarray;
}
