import Cookies from "/FrontEnd/includes/js/js.cookie.m.js";

export default async function authenticate(): Promise<boolean> {
    let authstatus = false;
    let username = Cookies.get("Username");
    let hashkey = Cookies.get("SessionKey");
    if (username == undefined || hashkey == undefined) {
        Cookies.remove("Username");
        Cookies.remove("SessionKey");
        return authstatus;
    }

    await $.post(
        "/BackEnd/php/ajax/client_auther.php",
        {
            'username': Cookies.get("Username"),
            'hashkey': Cookies.get("SessionKey")
        },
        (resp) => {
            console.log(resp);
            if (resp == "OKAUTH") {
                authstatus = true;
            }
            return;
        }
    )
    return authstatus;
}