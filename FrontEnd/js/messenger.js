import Cookies from "/FrontEnd/includes/js/js.cookie.m.js";
import authenticate from "./acctctrl.js";
const alphacodes = [
    "KeyQ",
    "KeyW",
    "KeyE",
    "KeyR",
    "KeyT",
    "KeyY",
    "KeyU",
    "KeyI",
    "KeyO",
    "KeyP",
    "KeyA",
    "KeyS",
    "KeyD",
    "KeyF",
    "KeyG",
    "KeyH",
    "KeyJ",
    "KeyK",
    "KeyL",
    "KeyZ",
    "KeyX",
    "KeyC",
    "KeyV",
    "KeyB",
    "KeyN",
    "KeyM"
];
const symbocodes = [
    "Backquote",
    "Minus",
    "Equal",
    "BracketLeft",
    "BracketRight",
    "Backslash",
    "Semicolon",
    "Quote",
    "Comma",
    "Period",
    "Slash",
    "Space",
    "NumpadSubtract",
    "NumpadAdd",
    "NumpadDivide",
    "NumpadDecimal",
    "NumpadMultiply"
];
const numericcodes = [
    "Digit1",
    "Digit2",
    "Digit3",
    "Digit4",
    "Digit5",
    "Digit6",
    "Digit7",
    "Digit8",
    "Digit9",
    "Digit0",
    "Numpad7",
    "Numpad8",
    "Numpad9",
    "Numpad4",
    "Numpad5",
    "Numpad6",
    "Numpad1",
    "Numpad2",
    "Numpad3",
    "Numpad0"
];
const entercodes = ["Enter", "NumpadEnter"];
const alphasymbonumericcodes = alphacodes.concat(symbocodes, numericcodes, entercodes);
export async function mock_onload() {
    const inbox = document.getElementsByClassName("inbox_list")[0];
    const history = document.getElementsByClassName("msg_history")[0];
    Cookies.set("Username", "Monkeyman");
    Cookies.remove("SessionKey");
    inbox.appendChild(await generate_element_conv("1", "no src", "Product", "UserSeller", "LastMessage", "LastTime", "LastType"));
    history.appendChild(generate_element_msg("1", "UserSeller", "Message1", "Date1", "textonly"));
    history.appendChild(generate_element_msg("1", "MonkeyMan", "Message2", "Date1", "textonly"));
    history.appendChild(generate_element_msg("1", "UserSeller", "Message3", "Date1", "offerdeal", "5000.00"));
    history.appendChild(generate_element_msg("1", "MonkeyMan", "Message4", "Date1", "offerdeal", "5000.00"));
}
export async function onload() {
    const rawsearchparams = window.location.href.split("?")[1];
    const searchparams = new URLSearchParams(rawsearchparams);
    if (searchparams.get("itemid") != null) {
    }
    if (Cookies.get("Username") != "Monkeyman" && Cookies.get("Username") === undefined || Cookies.get("SessionKey") === undefined) {
        alert("You are not logged in! Returning to homepage.");
        window.location.href = "/";
    }
    let is_auth = await authenticate();
    if (!is_auth) {
        alert("Session has expired! Returning to homepage.");
        return;
    }
    await populate_inbox();
    window.heartbeat_id = setInterval(() => {
        fetch_n_pull_inbox();
        if (!document.getElementsByClassName("inbox_item_active")[0]) {
            return;
        }
        fetch_n_pull_history();
    }, 2000);
    const txtbox = document.getElementsByClassName("text_area_textbox")[0];
    txtbox.disabled = true;
    window.addEventListener("keydown", (e) => {
        if (alphasymbonumericcodes.indexOf(e.code) <= -1) {
            return;
        }
        if (document.activeElement != txtbox) {
            txtbox.focus();
            if (entercodes.indexOf(e.code) > -1) {
                e.preventDefault();
            }
        }
    });
    txtbox.addEventListener("keydown", async (e) => {
        if (entercodes.indexOf(e.code) > -1 && !txtbox.disabled && txtbox.value.length > 0) {
            send_message(txtbox.value, "textonly", "");
            txtbox.value = "";
        }
    });
    const sendbtn = document.getElementsByClassName("msg_send_btn")[0];
    sendbtn.addEventListener("click", () => {
        if (txtbox.value.length > 0) {
            send_message(txtbox.value, "textonly", "");
            txtbox.value = "";
        }
    });
    let offerbutton = document.getElementById("makeoffer");
    offerbutton.addEventListener("click", () => { make_offer(); });
    let canceloffer = document.getElementById("canceloffer");
    canceloffer.addEventListener("click", () => { cancel_offer(); });
}
function generate_element_msg(message_id, sender, content, date, type, value = null) {
    const msg = document.createElement("div");
    msg.setAttribute("mid", message_id);
    msg.classList.add("msg");
    if (sender == Cookies.get("Username")) {
        msg.classList.add("outgoing");
    }
    else {
        msg.classList.add("incoming");
        const msg_img = document.createElement("div");
        msg_img.classList.add("msg_img");
        msg.appendChild(msg_img);
        const msg_img_img = document.getElementsByClassName("inbox_item_active")[0].getElementsByClassName("inbox_item_imgdiv")[0].getElementsByTagName("img")[0].cloneNode(true);
        msg_img.appendChild(msg_img_img);
    }
    const msg_content = document.createElement("div");
    msg_content.classList.add("msg_content");
    msg.appendChild(msg_content);
    const msg_text = document.createElement("p");
    msg_text.innerText = content;
    msg_content.appendChild(msg_text);
    switch (type) {
        case "offerdeal":
            msg.classList.add("offerdeal");
            const msg_price = document.createElement("p");
            msg_price.classList.add("msg_val");
            if (value == null) {
                console.warn("Offered deal is null price!");
                value = NaN.toString();
            }
            msg_price.innerText = "PHP " + value;
            msg_content.appendChild(msg_price);
            break;
        case "canceloffer":
            msg.classList.add("canceloffer");
            const msg_cancel = document.createElement("p");
            msg_cancel.classList.add("msg_val");
            msg_cancel.innerText = "Offer has been canceled.";
            msg_content.appendChild(msg_cancel);
            break;
        default:
            msg.classList.add("textonly");
            break;
    }
    const msg_date = document.createElement("p");
    msg_date.classList.add("msg_timestamp");
    msg_date.innerText = date;
    msg_content.appendChild(msg_date);
    return msg;
}
async function generate_element_conv(conv_id, img_src, whatis, whois, lastmessage, lastdate, lasttype) {
    console.log("Generating inbox for: ");
    console.log([conv_id, img_src, whatis, whois, lastmessage, lastdate, lasttype]);
    //Create base plate for conversation item
    const conv = document.createElement("div");
    conv.setAttribute("cid", conv_id);
    conv.classList.add("inbox_item");
    //Create img elements
    const conv_imgdiv = document.createElement("div");
    conv_imgdiv.classList.add("inbox_item_imgdiv");
    conv.appendChild(conv_imgdiv);
    const conv_img = document.createElement("img");
    conv_img.src = img_src;
    conv_img.alt = whatis;
    conv_imgdiv.appendChild(conv_img);
    //Create text elements
    const conv_bodydiv = document.createElement("div");
    conv_bodydiv.classList.add("inbox_item_bodydiv");
    conv.appendChild(conv_bodydiv);
    const conv_bodydiv_whatis = document.createElement("h5");
    const conv_bodydiv_whois = document.createElement("p");
    const conv_bodydiv_lastmsg = document.createElement("p");
    conv_bodydiv_lastmsg.classList.add("inbox_item_lastmessage");
    const conv_bodydiv_lastwhen = document.createElement("p");
    conv_bodydiv_lastwhen.classList.add("inbox_item_lastdate");
    conv_bodydiv_whatis.innerText = whatis;
    conv_bodydiv_whois.innerText = whois;
    switch (lasttype) {
        case null:
            conv_bodydiv_lastmsg.innerText = "No message yet...";
            break;
        case "offerdeal":
            conv_bodydiv_lastmsg.innerText = whois + " made an offer.";
            break;
        case "canceloffer":
            conv_bodydiv_lastmsg.innerText = whois + " canceled the offer.";
            break;
        default:
            conv_bodydiv_lastmsg.innerText = lastmessage;
            break;
    }
    conv_bodydiv_lastwhen.innerText = lastdate;
    conv_bodydiv.appendChild(conv_bodydiv_whatis);
    conv_bodydiv.appendChild(conv_bodydiv_whois);
    conv_bodydiv.appendChild(conv_bodydiv_lastmsg);
    conv_bodydiv.appendChild(conv_bodydiv_lastwhen);
    conv.addEventListener("click", () => {
        if (conv.classList.contains("inbox_item_active")) {
            return;
        }
        while (document.getElementsByClassName("inbox_item_active")[0]) {
            document.getElementsByClassName("inbox_item_active")[0].classList.remove("inbox_item_active");
        }
        conv.classList.add("inbox_item_active");
        const history = document.getElementsByClassName("msg_history")[0];
        while (history.firstChild) {
            history.removeChild(history.firstChild);
        }
        populate_history();
        document.getElementsByClassName("text_area_textbox")[0].disabled = false;
        $.post("/BackEnd/php/ajax/msg/messaging_getmessages.php", {
            "username": Cookies.get("Username"),
            "hashkey": Cookies.get("SessionKey"),
            "purpose": "checkofferstatus",
            "cid": Number(document.getElementsByClassName("inbox_item_active")[0].getAttribute("cid"))
        }, (resp) => {
            try {
                const msg = JSON.parse(resp);
                const offerscan = document.getElementById("offerscan");
                if (offerscan == null) {
                    console.error("Offer Scan element not existing!");
                    return;
                }
                switch (msg[1]) {
                    case "offerdeal":
                        offerscan.innerText = "PHP " + msg[2];
                        break;
                    case "canceloffer":
                        offerscan.innerText = "Offer has been canceled.";
                        break;
                    default:
                        offerscan.innerText = "No offer has been made yet...";
                        break;
                }
            }
            catch (err) {
                console.error("Server returned malformed response.");
                console.error(resp);
            }
        });
    });
    return conv;
}
async function populate_inbox() {
    $.post("/BackEnd/php/ajax/msg/messaging_getinbox.php", {
        "username": Cookies.get("Username"),
        "hashkey": Cookies.get("SessionKey")
    }, (resp) => {
        const inbox = document.getElementsByClassName("inbox_list")[0];
        try {
            const msgs = JSON.parse(resp);
            //$cid, $itemid, $itemname, $owner, $negotiator, $lastmessage, $lastwhen, $lasttype
            console.log(msgs);
            msgs.forEach(async (msg) => {
                inbox.appendChild(await generate_element_conv(msg[0], "null", msg[2], Cookies.get("Username") == msg[3] ? msg[4] : msg[3], msg[5], msg[6], msg[7]));
            });
        }
        catch (_a) {
            console.error(resp);
        }
    });
}
async function populate_history() {
    $.post("/BackEnd/php/ajax/msg/messaging_getmessages.php", {
        "username": Cookies.get("Username"),
        "hashkey": Cookies.get("SessionKey"),
        "purpose": "populate",
        "cid": parseInt(document.getElementsByClassName("inbox_item_active")[0].getAttribute("cid"))
    }, (resp) => {
        if (Cookies.get("Username") != "Monkeyman" && resp.startsWith("NOAUTH")) {
            console.log("Session has expired! Returning to homepage...");
            Cookies.remove("Username");
            Cookies.remove("SessionKey");
            window.location.href = "/";
            return;
        }
        const msgs = JSON.parse(resp);
        const history = document.getElementsByClassName("msg_history")[0];
        console.log(msgs);
        msgs.forEach(msg => {
            history.appendChild(generate_element_msg(msg[0], msg[1], msg[3], msg[2], msg[4], msg[5]));
        });
    });
}
async function fetch_n_pull_inbox() {
    $.post("/BackEnd/php/ajax/msg/messaging_getinbox.php", {
        "username": Cookies.get("Username"),
        "hashkey": Cookies.get("SessionKey"),
    }, async (resp) => {
        if (Cookies.get("Username") != "Monkeyman" && resp.startsWith("NOAUTH")) {
            console.log("Session has expired! Returning to homepage...");
            Cookies.remove("Username");
            Cookies.remove("SessionKey");
            window.location.href = "/";
            return;
        }
        if (resp.startsWith("<br />")) {
            console.error(resp);
            return;
        }
        const inbox = document.getElementsByClassName("inbox_list")[0];
        const msgs = JSON.parse(resp);
        msgs.reverse();
        const inboxtemp = Array.from(inbox.children);
        for (let i = 0; i < msgs.length; i++) {
            const msg = msgs[i];
            let isfound = false;
            inboxtemp.forEach(async (el) => {
                if (isfound) {
                    return;
                }
                if (el.getAttribute("cid") == msg[0]) {
                    inbox.removeChild(el);
                    inbox.appendChild(el);
                    inboxtemp.splice(inboxtemp.indexOf(el), 1);
                    isfound = true;
                }
            });
            if (isfound) {
                return;
            }
            else {
                inbox.appendChild(await generate_element_conv(msg[0], "null", msg[3], Cookies.get("Username") == msg[1] ? msg[2] : msg[1], msg[4], msg[5], msg[6]));
                return;
            }
        }
    });
}
async function fetch_n_pull_history() {
    var _a;
    $.post("/BackEnd/php/ajax/msg/messaging_getmessages.php", {
        "username": Cookies.get("Username"),
        "hashkey": Cookies.get("SessionKey"),
        "purpose": "append",
        "cid": Number(document.getElementsByClassName("inbox_item_active")[0].getAttribute("cid")),
        "mid": Number((_a = document.getElementsByClassName("msg_history")[0].firstElementChild) === null || _a === void 0 ? void 0 : _a.getAttribute("mid")),
    }, (resp) => {
        try {
            const msgs = JSON.parse(resp);
            const history = document.getElementsByClassName("msg_history")[0];
            for (const msg of msgs) {
                const new_msg_el = generate_element_msg(msg[0], msg[1], msg[3], msg[2], msg[4], msg[5]);
                history.prepend(new_msg_el);
            }
        }
        catch (error) {
            console.error("Server returned malformed data. " + resp);
        }
    });
    $.post("/BackEnd/php/ajax/msg/messaging_getmessages.php", {
        "username": Cookies.get("Username"),
        "hashkey": Cookies.get("SessionKey"),
        "purpose": "checkofferstatus",
        "cid": Number(document.getElementsByClassName("inbox_item_active")[0].getAttribute("cid"))
    }, (resp) => {
        try {
            const msg = JSON.parse(resp);
            const offerscan = document.getElementById("offerscan");
            if (offerscan == null) {
                console.error("Offer Scan element not existing!");
                return;
            }
            switch (msg[1]) {
                case "offerdeal":
                    offerscan.innerText = "PHP " + msg[2];
                    break;
                case "canceloffer":
                    offerscan.innerText = "Offer has been canceled.";
                    break;
                default:
                    offerscan.innerText = "No offer has been made yet...";
                    break;
            }
        }
        catch (err) {
            console.error("Server returned malformed response.");
            console.error(resp);
        }
    });
}
async function send_message(content, type, value) {
    $.post("/BackEnd/php/ajax/msg/messaging_sendmessage.php", {
        "username": Cookies.get("Username"),
        "hashkey": Cookies.get("SessionKey"),
        "convo": Number(document.getElementsByClassName("inbox_item_active")[0].getAttribute("cid")),
        "content": content,
        "type": type,
        "value": value,
    }, (resp) => {
        console.info(resp);
    });
}
async function make_offer() {
    let askprice = prompt("How much are you asking for? ", "CANCEL");
    if (askprice == "CANCEL" || askprice == null) {
        return;
    }
    let heck = parseFloat(askprice);
    if (Number.isNaN(heck)) {
        alert("You entered an invalid asking price.");
        return;
    }
    let confirmation = confirm("Are you sure you want to offer PHP " + askprice + " for this item?");
    if (!confirmation) {
        console.log("BREAK!");
        return;
    }
    send_message("", "offerdeal", askprice.toString());
}
async function cancel_offer() {
    let confirmation = confirm("Are you sure you want to cancel your previous offer?");
    if (!confirmation) {
        return;
    }
    send_message("", "canceloffer", "");
}
export async function create_new_convo($itemid) {
    $.post("/BackEnd/php/ajax/msg/messaging_makeconvo.php", {
        "username": Cookies.get("Username"),
        "hashkey": Cookies.get("SessionKey"),
        "itemid": $itemid
    }, (resp) => {
        if (resp.startsWith("SUCCESS:NewConv ")) {
            let convo = resp.replace("SUCCESS:NewConv ", "");
            window.location.href = "/FrontEnd/messenger.html?target_conv=" + convo;
            return;
        }
        if (resp.startsWith("NOTE:ConvAlreadyExists ")) {
            let convo = resp.replace("NOTE:ConvAlreadyExists ", "");
            window.location.href = "/FrontEnd/messenger.html?target_conv=" + convo;
            return;
        }
        else {
            console.error("Creating convo failed. ");
            console.info(resp);
        }
    });
}
