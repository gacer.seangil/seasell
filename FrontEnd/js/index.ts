function includeHTML() {
    var z, i, elmnt, file, xhttp;

    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];

        file = elmnt.getAttribute("page_load");
        if (file) {

            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) {elmnt.innerHTML = this.responseText;}
                    if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
                    elmnt.removeAttribute("page_load");
                    includeHTML();
                }
            }      
        xhttp.open("GET", file, true);
        xhttp.send();

        return;
        }
    }
};

function create_category_btn (button_title : string ) { //TODO
    let btn = document.createElement("div");
    btn.classList.add("box-cat-1");
    btn.addEventListener("click",()=>{});
}

function change_color_btn(a){
    const num = [0,1,2,3,4];

    for (let i in num){
        document.getElementById(i).style.backgroundColor = "";
        document.getElementById(i).style.color = "#008fee";
    }
    document.getElementById(a).style.backgroundColor = "#008fee";
    document.getElementById(a).style.color = "white";
}

function next(a){
    window.location.href = "/FrontEnd/" + a;
}