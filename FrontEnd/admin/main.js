import Cookies from "/FrontEnd/includes/js/js.cookie.m.js";
// INITIALIZERS
async function init_all() {
    let logobar = document.createElement("div");
    logobar.classList.add("titleplate");
    let icon = document.createElement("i");
    icon.classList.add("ri-menu-line");
    let hehe = document.createElement("h2");
    let hehe_bluepart = document.createElement("span");
    hehe_bluepart.append("Sell");
    hehe.append("Sea", hehe_bluepart);
    logobar.append(icon, hehe);
    document.getElementsByClassName("header")[0].prepend(logobar);
    let sidebar = document.querySelector('.sidebar');
    let mainContent = document.querySelector('.main--content');
    logobar.addEventListener("click", () => {
        sidebar === null || sidebar === void 0 ? void 0 : sidebar.classList.toggle("active");
        mainContent === null || mainContent === void 0 ? void 0 : mainContent.classList.toggle("active");
    });
}
export async function init_dashboard() {
    init_all();
    let overviewtarget = document.getElementsByClassName("overview")[0];
    let cards = overviewtarget.getElementsByClassName("cards")[0];
    window.UsersPanel = new DashboardPanelDataHolderUsers();
    window.ProductPanel = new DashboardPanelDataHolderProducts();
    cards.append(window.UsersPanel.panel);
    cards.append(window.ProductPanel.panel);
    setInterval(async () => {
        for (let i = 0; i < [window.UsersPanel, window.ProductPanel].length; i++) {
            const panel = [window.UsersPanel, window.ProductPanel][i];
            await panel.datalistener();
        }
    }, 5000);
}
export async function init_userspage() {
    init_all();
    let main_thingie = document.getElementsByClassName("main--content")[0];
    window.UsersTable = new UsersTable();
    main_thingie.appendChild(window.UsersTable.table);
    window.UsersTable.fetch_data();
}
export async function init_productspage() {
    init_all();
    let main_thingie = document.getElementsByClassName("main--content")[0];
    window.ProductTable = new ProductsTable();
    main_thingie.appendChild(window.ProductTable.table);
    window.ProductTable.fetch_data();
}
// DATA MANAGERS
// For `adminpanel.html`
class DashboardPanelDataHolder {
    constructor() {
        this.panel = document.createElement("div");
        this.panel.classList.add("card");
        let overview = document.createElement("div");
        overview.classList.add("card-overview");
        let summary = document.createElement("div");
        summary.classList.add("card-summary");
        overview.appendChild(summary);
        this.summaryh5 = document.createElement("h5");
        this.summaryh5.classList.add("card--title");
        summary.appendChild(this.summaryh5);
        this.summaryh1 = document.createElement("h1");
        summary.appendChild(this.summaryh1);
        this.icon = document.createElement("i");
        overview.appendChild(this.icon);
        this.stats = document.createElement("div");
        this.stats.classList.add("card-stats");
        this.panel.append(overview, this.stats);
        this.datalistener();
    }
}
class DashboardPanelDataHolderUsers extends DashboardPanelDataHolder {
    constructor() {
        super();
        this.panel.classList.add("card-user");
        this.summaryh5.append("Users");
        this.summaryh1.id = "product-total";
        this.summaryh1.append("0");
        this.icon.classList.add("ri-user-2-line", "card--icon--lg");
    }
    async datalistener() {
        await $.post("/BackEnd/php/ajax/admin-tools/admin_fetcher.php", {
            "username": Cookies.get("Username"),
            "hashkey": Cookies.get("SessionKey"),
            "requests": [
                "count_users"
            ]
        }, (resp) => {
            const incomingdata = JSON.parse(resp);
            this.summaryh1.innerText = incomingdata[0][0];
        });
    }
    ;
}
class DashboardPanelDataHolderProducts extends DashboardPanelDataHolder {
    constructor() {
        super();
        this.panel.classList.add("card-products");
        this.summaryh5.append("Products");
        this.summaryh1.id = "product-total";
        this.summaryh1.append("0");
        this.icon.classList.add("ri-product-hunt-line", "card--icon--lg");
        this.details = [
            [document.createElement("i"), document.createElement("div"), document.createElement("div")],
            [document.createElement("i"), document.createElement("div"), document.createElement("div")],
            [document.createElement("i"), document.createElement("div"), document.createElement("div")],
            [document.createElement("i"), document.createElement("div"), document.createElement("div")],
            [document.createElement("i"), document.createElement("div"), document.createElement("div")]
        ];
        this.details[0][0].classList.add("ri-question-mark");
        this.details[1][0].classList.add("ri-eye-line");
        this.details[2][0].classList.add("ri-eye-off-line");
        this.details[3][0].classList.add("ri-close-line");
        this.details[4][0].classList.add("ri-check-double-line");
        this.details[0][1].append(this.details[0][0], "Pending: ");
        this.details[1][1].append(this.details[1][0], "Active: ");
        this.details[2][1].append(this.details[2][0], "Inactive: ");
        this.details[3][1].append(this.details[3][0], "Rejected: ");
        this.details[4][1].append(this.details[4][0], "Sold Out: ");
        this.details[0][2].append("0");
        this.details[0][2].id = "statistics-items-pending";
        this.details[1][2].append("0");
        this.details[1][2].id = "statistics-items-active";
        this.details[2][2].append("0");
        this.details[2][2].id = "statistics-items-inactive";
        this.details[3][2].append("0");
        this.details[3][2].id = "statistics-items-rejected";
        this.details[4][2].append("0");
        this.details[4][2].id = "statistics-items-soldout";
        this.stats.append(this.details[0][1], this.details[0][2], this.details[1][1], this.details[1][2], this.details[2][1], this.details[2][2], this.details[3][1], this.details[3][2], this.details[4][1], this.details[4][2]);
    }
    async datalistener() {
        await $.post("/BackEnd/php/ajax/admin-tools/admin_fetcher.php", {
            "username": Cookies.get("Username"),
            "hashkey": Cookies.get("SessionKey"),
            "requests": [
                "get_items_summary_all_all",
                "get_items_summary_all_pending",
                "get_items_summary_all_rejected",
                "get_items_summary_all_active",
                "get_items_summary_all_inactive",
                "get_items_summary_all_soldout"
            ]
        }, (resp) => {
            const incomingdata = JSON.parse(resp);
            this.summaryh1.innerText = incomingdata[0][0];
            this.details[0][2].innerText = incomingdata[1][0];
            this.details[1][2].innerText = incomingdata[2][0];
            this.details[2][2].innerText = incomingdata[3][0];
            this.details[3][2].innerText = incomingdata[4][0];
            this.details[4][2].innerText = incomingdata[5][0];
        });
        return;
    }
}
class AdminMonitorTable {
    fetch_data() {
        while (this.tablebody.lastChild) {
            this.tablebody.removeChild(this.tablebody.lastChild);
        }
        return;
        // extend code to grab database data
    }
    forward() {
        this.offset += this.limit;
        this.fetch_data;
    }
    backward() {
        this.offset -= this.limit;
        this.fetch_data();
    }
    constructor() {
        this.limit = 20;
        this.offset = 0;
        this.table = document.createElement("table");
        this.tablebody = document.createElement("tbody");
        this.table.appendChild(this.tablebody);
        this.ctrldiv = document.createElement("div");
        this.ctrldiv.classList.add("btn-ctrls");
        this.table.appendChild(this.ctrldiv);
        let backwardbtn = document.createElement("button");
        backwardbtn.type = "button";
        backwardbtn.innerText = "<< Back";
        this.ctrldiv.append(backwardbtn);
        let forwardbtn = document.createElement("button");
        forwardbtn.type = "button";
        forwardbtn.innerText = "Fore >>";
        this.ctrldiv.append(forwardbtn);
    }
}
class ProductsTable extends AdminMonitorTable {
    constructor() {
        super();
        this.ctrldiv.children[0].addEventListener("click", () => {
            this.offset -= this.limit;
            if (this.offset < 0)
                this.offset = 0;
            this.fetch_data();
        });
        this.ctrldiv.children[1].addEventListener("click", () => {
            this.offset += this.limit;
            this.fetch_data();
        });
    }
    async fetch_data() {
        super.fetch_data();
        await $.post("/BackEnd/php/ajax/admin-tools/admin_fetcher.php", {
            "username": Cookies.get("Username"),
            "hashkey": Cookies.get("SessionKey"),
            "requests": ["get_items_all_all"],
            "bindparamtypes": "ii",
            "bindparams": [this.limit, this.offset]
        }, (resp) => {
            let answer = JSON.parse(resp);
            console.log(answer);
            while (answer.length < this.limit) {
                answer.push(["", "", "", "", "", "", "", ""]);
            }
            let headerrowlabels = ["Item ID", "Seller", "Item", "Category", "Desription", "Price", "Quantity", "Status", "Actions"];
            let trh = document.createElement("tr");
            trh.classList.add("header-row");
            this.tablebody.appendChild(trh);
            for (let i = 0; i < headerrowlabels.length; i++) {
                const label = headerrowlabels[i];
                let trhd = document.createElement("td");
                trhd.append(label);
                trh.append(trhd);
            }
            for (let i = 0; i < answer.length; i++) {
                const row = answer[i];
                let is_data = false;
                if (row[0] != "") {
                    is_data = true;
                }
                let tr = document.createElement("tr");
                this.tablebody.append(tr);
                for (let i = 0; i < row.length; i++) {
                    const cell = row[i];
                    let td = document.createElement("td");
                    td.append(cell);
                    tr.appendChild(td);
                }
                let tda = document.createElement("td");
                tda.classList.add("row-actions");
                tr.appendChild(tda);
                if (is_data) {
                    let banishbutton = document.createElement("button");
                    banishbutton.type = "button";
                    banishbutton.classList.add("banish");
                    banishbutton.innerText = "Reject";
                    let summonbutton = document.createElement("button");
                    summonbutton.type = "button";
                    summonbutton.classList.add("summon");
                    summonbutton.innerText = "Accept";
                    banishbutton.addEventListener("click", async () => {
                        console.log("BANISHING ITEM");
                        await $.post("/BackEnd/php/ajax/admin-tools/item-action.php", {
                            "username": Cookies.get("Username"),
                            "hashkey": Cookies.get("SessionKey"),
                            "item": row[0],
                            "action": "reject"
                        }, (resp) => {
                            if (resp.startsWith("NOAUTH")) {
                                console.error("NOT AUTHORIZED");
                                return;
                            }
                            console.log(resp);
                            this.fetch_data();
                            return;
                        });
                    });
                    summonbutton.addEventListener("click", () => {
                        console.log("UPDATING ITEM STATUS");
                        $.post("/BackEnd/php/ajax/admin-tools/item-action.php", {
                            "username": Cookies.get("Username"),
                            "hashkey": Cookies.get("SessionKey"),
                            "item": row[0],
                            "action": "accept"
                        }, (resp) => {
                            if (resp.startsWith("NOAUTH")) {
                                console.error("NOT AUTHORIZED");
                                return;
                            }
                            console.log(resp);
                            this.fetch_data();
                            return;
                        });
                    });
                    tda.appendChild(banishbutton);
                    tda.appendChild(summonbutton);
                }
            }
        });
        return;
    }
}
class UsersTable extends AdminMonitorTable {
    constructor() {
        super();
        this.ctrldiv.children[0].addEventListener("click", () => {
            this.offset -= this.limit;
            if (this.offset < 0)
                this.offset = 0;
            this.fetch_data();
        });
        this.ctrldiv.children[1].addEventListener("click", () => {
            this.offset += this.limit;
            this.fetch_data();
        });
    }
    async fetch_data() {
        super.fetch_data();
        await $.post("/BackEnd/php/ajax/admin-tools/admin_fetcher.php", {
            "username": Cookies.get("Username"),
            "hashkey": Cookies.get("SessionKey"),
            "requests": ["get_users"],
            "bindparamtypes": "ii",
            "bindparams": [this.limit, this.offset]
        }, (resp) => {
            let answer = JSON.parse(resp);
            console.log(answer);
            while (answer.length < this.limit) {
                answer.push(["", "", "", "", "", "", "", "", ""]);
            }
            let headerrowlabels = ["User ID", "Username", "Email", "Country", "Region", "City", "Type", "Elevation", "Date Joined", "Actions"];
            let trh = document.createElement("tr");
            trh.classList.add("header-row");
            this.tablebody.appendChild(trh);
            for (let i = 0; i < headerrowlabels.length; i++) {
                const label = headerrowlabels[i];
                let trhd = document.createElement("td");
                trhd.append(label);
                trh.append(trhd);
            }
            for (let i = 0; i < answer.length; i++) {
                const row = answer[i];
                let is_data = false;
                if (row[0] != "") {
                    is_data = true;
                }
                let tr = document.createElement("tr");
                this.tablebody.append(tr);
                for (let i = 0; i < row.length; i++) {
                    const cell = row[i];
                    let td = document.createElement("td");
                    td.append(cell);
                    tr.appendChild(td);
                }
                let tda = document.createElement("td");
                tda.classList.add("row-actions");
                tr.appendChild(tda);
                if (is_data) {
                    let banishbutton = document.createElement("button");
                    banishbutton.type = "button";
                    banishbutton.classList.add("ban");
                    banishbutton.innerText = "Ban";
                    let gotobutton = document.createElement("button");
                    gotobutton.type = "button";
                    gotobutton.classList.add("visit");
                    gotobutton.innerText = "Visit Profile";
                    banishbutton.addEventListener("click", async () => {
                        await $.post("/BackEnd/php/ajax/admin-tools/user-action.php", {
                            "username": Cookies.get("Username"),
                            "hashkey": Cookies.get("SessionKey"),
                            "user": row[0],
                            "action": "ban"
                        }, (resp) => {
                            if (resp.startsWith("NOAUTH")) {
                                console.error("NOT AUTHORIZED");
                                return;
                            }
                            console.log(resp);
                            this.fetch_data();
                            return;
                        });
                    });
                    tda.appendChild(banishbutton);
                    tda.appendChild(gotobutton);
                }
            }
        });
        return;
    }
}
